# Shuffle and dump bam files to paired fastq.gz files
#
# Jeff Stafford - CAC


from glob import glob

# all fastq names
FASTQS = glob_wildcards('bam-init/{fastq}.bam').fastq 
# unique sample names only
SAMPLES = sorted(list({x for x in glob_wildcards('bam-init/{garbage}.{sample}.bam').sample}))
READS = ['R1', 'R2']

localrules: all, clean

# final pipeline output
rule all:
    input:  
        expand('fastq/{sample}_{read}.fastq.gz', sample=SAMPLES, read=READS)


# nuke everything and start over
rule clean:
    shell:
        '''
        rm -rf fastq-tkns
        '''


# a dummy rule to make parallelization easier
rule dump_by_sample_dummy:
    input:  expand('bam-init/{fastq}.bam', fastq=FASTQS)
    output: expand('fastq-tkn/{sample}.tkn', sample=SAMPLES)
    shell:  'touch {output}'


# Many bam files were separated by lane, combine them back together again,
# shuffle bam, then dump all to fastq.gz
rule dump_by_sample_worker:
    input:  'fastq-tkn/{sample}.tkn'
    output: 
        r1='fastq/{sample}_%s.fastq.gz' % READS[0],
        r2='fastq/{sample}_%s.fastq.gz' % READS[1]
    log:    'fastq/{sample}-dump.log'
    shell:
        '''
        STEM=$(basename {input} .tkn)
        BAM_FILES=$(ls bam-init/*${{STEM}}.bam)
        samtools cat $BAM_FILES | \
            samtools collate -n 128 -O -u /dev/stdin $TMPDIR/$STEM | \
            java -Xmx40g -Djava.io.tmpdir=$TMPDIR -XX:ParallelGCThreads=1 -jar /opt/picardtools/2.0.1/picard.jar \
                SamToFastq VALIDATION_STRINGENCY=SILENT INPUT=/dev/stdin FASTQ={output.r1} SECOND_END_FASTQ={output.r2} &> {log}
        '''

