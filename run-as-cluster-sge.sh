#!/bin/bash

source activate rnaseq

# extract from bam
#nohup snakemake --nolock -j 100 --cluster-config cluster.json -s Snakefile-extract-bam \
#    --cluster 'qsub -V -cwd -pe shm.pe {threads} -l mf={cluster.mem} -o misc-log.log -j y' & 
# run pipeline
nohup snakemake --nolock -j 100 --cluster-config cluster.json \
    --cluster 'qsub -V -cwd -pe shm.pe {threads} -l mf={cluster.mem} -o misc-log.log -j y' & 



