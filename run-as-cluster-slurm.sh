#!/bin/bash

source activate rnaseq
# run pipeline
nohup snakemake --nolock -j 100 --cluster-config cluster.json \
    --cluster 'sbatch -c {threads} --mem {cluster.mem} -t {cluster.time} -o misc-log.log' & 



