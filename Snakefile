# Computes differential rna-seq expression using the "new" tuxedo suite.
#
# Jeff Stafford - CAC


from glob import glob

SAMPLES, READS = glob_wildcards('fastq/{sample}_{read}.fastq.gz')
SAMPLES = list(set(SAMPLES))
READS = list(set(READS))
FASTQS = glob_wildcards('fastq/{fastq}.fastq.gz').fastq

# can specify references here
REF_GTF = glob('ref/*.gtf')
REF_FA = glob('ref/*.fa')

localrules: all, clean, multiqc_raw, multiqc_postqc, trim_by_sample_dummy

# final pipeline output
rule all:
    input:  
            'fastq/multiqc_report.html',
            'fastq-trimmed/multiqc_report.html',
#            expand('counts/{sample}/{sample}.gtf', sample=SAMPLES),
            'counts/all_counts.csv'


# nuke everything and start over
rule clean:
    shell:
        '''
        rm -rf fastq-trimmed alignments alignments assemblies counts
        '''


# get qc scores from raw fastq files
rule fastqc:
    input:  '{dir}/{sample}.fastq.gz'
    output: '{dir}/{sample}_fastqc.html'
    log:    '{dir}/{sample}-fastqc.log'
    shell:  'fastqc {input} -d $TMPDIR &> {log}'


# aggregate QC info on "fastq/" folder (raw fastqs by sample)
rule multiqc_raw:
    input:  expand('fastq/{sample}_fastqc.html', sample=FASTQS) 
    output: 'fastq/multiqc_report.html'
    shell:  'multiqc -f -o $(dirname {output}) $(dirname {output})'


# aggregate QC info on "fastq-trimmed/" folder (post-adapter/quality trim)
rule multiqc_postqc:
    input:  expand('fastq-trimmed/{sample}_fastqc.html', sample=FASTQS)
    output: 'fastq-trimmed/multiqc_report.html'
    shell:  'multiqc -f -o $(dirname {output}) $(dirname {output})'


rule trim_adapters:
    input:  
        r1='fastq/{sample}_%s.fastq.gz' % READS[0],
        r2='fastq/{sample}_%s.fastq.gz' % READS[1]
    output: 
        r1='fastq-trimmed/{sample}_%s.fastq.gz' % READS[0],
        r2='fastq-trimmed/{sample}_%s.fastq.gz' % READS[1]
    log:    'fastq-trimmed/{sample}-trim_adapters.log'
    shell:  'cutadapt -m 20 -q 30 -a AGATCGGAAGAG -A AGATCGGAAGAG -o {output.r1} -p {output.r2} {input.r1} {input.r2} &> {log}'


# build index for HISAT2
rule hisat2_index:
    input:  
        fa=REF_FA,
        gtf=REF_GTF
    output: 
        exon='ref/exon.hisat2',
        ss='ref/ss.hisat2',
        token='ref/index-complete.hisat2'
    threads: 10
    log:    'ref/hisat2-index.log'
    shell:
        '''
        hisat2_extract_exons.py -v {input.gtf} 1> {output.exon} 2> {log} &&
            hisat2_extract_splice_sites.py -v {input.gtf} 1> {output.ss} 2>> {log} &&
            hisat2-build --exon {output.exon} --ss {output.ss} {input.fa} ref/hisat2-index &>> {log} &&
            touch {output.token}
        '''


# Align reads to reference using HISAT2
rule hisat2_align:
    input:
        fq1='fastq-trimmed/{sample}_%s.fastq.gz' % READS[0],
        fq2='fastq-trimmed/{sample}_%s.fastq.gz' % READS[1],
        index_tkn='ref/index-complete.hisat2'
    output: 'alignments/{sample}.bam'
    threads: 8
    log:    'alignments/{sample}-hisat2_align.log'
    shell:  
        '''
        hisat2 -p {threads} --dta --no-unal -t -x ref/hisat2-index -1 {input.fq1} -2 {input.fq2} 2> {log} | \
            samtools sort -m 8G - > {output}
        '''


# Transcriptome assembly
rule stringtie_assemble:
    input:  
        bam='alignments/{sample}.bam',
        gtf=REF_GTF
    output: 'assemblies/{sample}.gtf'
    threads: 8
    log:    'assemblies/{sample}-stringtie_assemble.log'
    shell:  'stringtie -p {threads} -l $(basename {output} .gtf) -G {input.gtf} {input.bam} 1> {output} 2> {log}'
    

# Merge all assemblies into one glorious master assembly
rule stringtie_merge:
    input:  
        smp_gtf=expand('assemblies/{sample}.gtf', sample=SAMPLES),
        guide_gtf=REF_GTF
    output: 'assemblies/merged.gtf'
    threads: 16
    log:    'assemblies/stringtie_merge.log'
    shell:  
        '''
        echo {input.smp_gtf} | sed 's/ /\\n/g' > assemblies/mergelist.txt &&
            stringtie --merge -p {threads} -G {input.guide_gtf} assemblies/mergelist.txt 1> {output} 2> {log}
        '''


# Get read counts from alignments using our master assembly
rule stringtie_count:
    input:
        bam='alignments/{sample}.bam',
        gtf='assemblies/merged.gtf'
    output: 'counts/{sample}/{sample}.gtf'
    threads: 8
    log:    'counts/{sample}-stringtie_count.log'
    shell:  'stringtie -p 8 -e -B -G {input.gtf} -o {output} {input.bam} &> {log}'


# Get read counts using featurecounts for differential expression using edgeR. 
rule featurecounts:
    input:
        bam='alignments/{sample}.bam',
        gtf='assemblies/merged.gtf'
    output: 'counts/raw/{sample}.tsv'
    threads: 4
    log:    'counts/raw/{sample}-featurecounts.log'
    shell:  'featureCounts -T {threads} -p -t exon -g gene_id -a {input.gtf} -o {output} {input.bam} &> {log}'


# aggregate all counts to counts/all_counts.csv
rule aggregate_counts:
    input:  
        expand('counts/raw/{sample}.tsv', sample=SAMPLES),
        'sample_info.csv'
    output: 'counts/all_counts.csv'
    log:    'counts/aggregate_counts.log'
    shell:  'Rscript scripts/aggregate-counts.R &> {log}'


