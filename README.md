# rnaseq-diffexp

This is a pipeline designed to automatically analyze RNA sequencing data and compute differential expression between experimental conditions.
Briefly, reads are aligned using HISAT2 and a reference-guided transcriptome assembly is produced using StringTie. 
Raw count data is produced from the aligned reads using featureCounts, and then differential expression calls are performed using edgeR. 
This closely mirrors the "new Tuxedo Suite" protocol from http://www.nature.com/nprot/journal/v11/n9/full/nprot.2016.095.html, 
the main exception being the use of edgeR to compute differential expression.

## Installing the pipeline

* Download it to the cluster (typically using the `git clone` command at the top of the Bitbucket repository). 

* Retrieve your genomic references and put them in the `ref/` folder. Alternatively, running the `get-ref.sh` script (in the `ref/` folder) will download the Human genome for you.

* You can install the full set of software used for this analysis using Anaconda/Miniconda.

To install Miniconda:

```{bash}
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# answer "yes" whenever prompted
source ~/.bashrc
```

To install the pipeline environment:

```{bash}
conda update --all
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
conda env create -f rnaseq-conda-env.yaml
```

To load all of the pipeline software on demand:

```{bash}
source activate rnaseq
```

## Running the pipeline

* Move your FASTQ read files to the `fastq/` folder (you will need to create this folder if it does not exist).

* Create the file `sample_info.csv`. The first column must be called `id` and have the sample names. Other columns can be anything you want and typically contain sample categories.

* Run `source run-as-cluster-slurm.sh` (on a cluster that uses SLURM) or modify it to fit your use case. The pipeline will produce an `counts/all_counts.csv` when done.

* To perform a differential expression call for a given condition, run `Rscript scripts/edgeR-exactTest.R -c condition -o output_directory denominator,numerator`. `condition` is a column from `sample_info.csv` and `numerator` and `denominator` are sample categories from this condition. Positive-fold change indicates a gene was overexpressed in "numerator" samples.

